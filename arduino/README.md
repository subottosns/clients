# Gestione dell'arduino
Durante la 24h, l'arduino parla con una macchina, chiamata interfaccia, per
scambiarsi info sui punteggi, gol fatti, etc.
Per farlo, bisogna fare setup di due cose, l'arduino e l'interfaccia. In
particolare, la cosa cancro è che entrambi devono conoscere l'ip dell'altro.

## Arduino
Lo sketch (programma di arduino) per la 24h è in `arduino_scripts/arduino2019`.
* Mettere l'ip della macchina interfaccia in `config.h` (eventualmente
  crearlo copiando `config.h.example`), variabile `server_ip`
* Flashare lo script `arduino2019.ino` sull'arduino

NOTA: questo vuol dire che se, per qualsiasi motivo, la macchina interfaccia
cambia IP bisogna riflashare l'arduino. Non fatelo succedere durante la 24 ore.

## Fotocellule
Sono composte di un led e di un fotodiodo.
Il led ha 2 pin: uno alla GND (lungo) e uno al fotodiodo (fotodiodo). Schemino:
    ____ 
   |    |
   _    |
  |/    |
  _     |
  v     |
  |     |
  foto  |
        |
       GND

Il fotodiodo ha 4 pin che letti in qualche ordine rispettano i colori White, Red, Blue, Red (che va al led).

## Interfaccia
Lo script da avviare è `arduino_interface.py`. Richiede libsubotto per
funzionare. Ha un po' di opzioni (vedi la sua doc), ma dovrebbe bastare
`python arduino_interface.py <id 24h> <ip arduino>`

## Problemi, trucchi e workaround
### Ottenere l'ip dell'arduino
Visto che non si può eseguire `ip a` sull'arduino, servono modi alternativi di
ottenere il suo ip. Una possibilità è ascoltare i pacchetti ricevuti
dall'interfaccia, e vedere da chi arrivano. In particolare, avviando
`arduino_interface.py` con un ip a caso e poi l'arduino, si dovrebbero ricevere
i suoi pacchetti, che lo script loggherà a schermo dicendo che li ha rifiutati.
Questo però permetterà anche di vedere l'ip dell'arduino, con cui poi si può
riavviare lo script (aspettando 2 minuti, vedere sopra).

### Ignorare fotocellule esplose
Se ad un certo punto una fotocellula esplode e inizia a sparare millemila gol,
è possibile ignorarla dall'interfaccia. Il log stampato a schermo dovrebbe dire
che evento sta esplodento. Bisogna aggiungere il codice da ignorare alla lista
nel file `ignore_codes`, nella cartella da cui viene avviata l'interfaccia. Un
`ignore_codes` di esempio è in questa cartella, comunque il formato è una lista
di numeri separati da virgole.

# Architettura generale
```
|---------|                |-----------|                |----|
| arduino | <-- TCP/IP --> | interface | <-- TCP/IP --> | DB |
|---------|                |-----------|                |----|
```

L'arduino non parla direttamente con il db, ma parla con una macchina che funge da
interfaccia tramite lo script `arduino_interface.py`. L'arduino e l'interfaccia si
scambiano informazioni sullo stato della partita (gol fatti, subottoni, punteggi,
cambi, etc.) tramite TCP/IP. L'interfaccia poi si occupa di mandare tutte le
informazioni al db.

# TODO
* migliorare la gestione degli ip, in particolare non sarebbe male non aver
  bisogno dell'ip dell'arduino
