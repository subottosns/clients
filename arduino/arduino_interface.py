#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import socket
import socketserver
import time
import threading
import struct
import argparse

from libsubotto.core import SubottoCore

running = True
dry_run = False

ANTISPAM_TIMER = 0.5
SHOW_PARTIAL_TIME = 3

# Always hold this lock to access the core
core_lock = threading.Lock()
core = None

CODE_NOOP = 0
CODE_CELL_RED_PLAIN = 1
CODE_CELL_RED_SUPER = 2
CODE_CELL_BLUE_PLAIN = 3
CODE_CELL_BLUE_SUPER = 4
CODE_BUTTON_RED_GOAL = 7
CODE_BUTTON_RED_UNDO = 8
CODE_BUTTON_BLUE_GOAL = 5
CODE_BUTTON_BLUE_UNDO = 6

ignore_codes = []

allowed_IPs = ['127.0.0.1']
socketserver.TCPServer.allow_reuse_address = True


def timezone():
    t = int(time.time())
    return (t % 10 < 5)

def check_time(last_gol):
    if last_gol is None:
        return 1
    return time.time()-last_gol.timestamp()

def reload_ignore_codes():
    """Reload ignore codes from file. In case of errors with the file, returns None."""
    try:
        with open('./ignore_codes', 'r') as ignore_file:
            content = ignore_file.readline()
        return list(map(int, filter(lambda s: s != "", content.split(','))))
    except FileNotFoundError:
        print("Warning: file ignore_codes not found. Default to no ignored code", file=sys.stderr)
        return []
    except ValueError as e:
        print("Errors with ignore_codes file: " + str(e), file=sys.stderr)
        print("Maybe it's in the wrong format?", file=sys.stderr)
    except:
        print("File ignore_codes is broken in an unexpected way (you shouldn't see this)", file=sys.stderr)


# From https://docs.python.org/2/library/socketserver.html#asynchronous-mixins
class Connection(socketserver.BaseRequestHandler):
    def handle(self):
        # TODO: print peer name
        print("Connection received", file=sys.stderr)
        addr = self.client_address
        print(addr, file=sys.stderr)
        global running, core, core_lock, allowed_IPs, ignore_codes
        if addr[0] not in allowed_IPs:
            print("shutting down", allowed_IPs, file=sys.stderr)
            self.request.shutdown(socket.SHUT_RDWR)
            return
        actions = {
            CODE_NOOP: lambda: None,
            CODE_CELL_RED_PLAIN: core.easy_act_red_goal_cell,
            CODE_CELL_RED_SUPER: core.easy_act_red_supergoal_cell,
            CODE_CELL_BLUE_PLAIN: core.easy_act_blue_goal_cell,
            CODE_CELL_BLUE_SUPER: core.easy_act_blue_supergoal_cell,
            CODE_BUTTON_RED_GOAL: core.easy_act_red_goal_button,
            CODE_BUTTON_RED_UNDO: core.easy_act_red_goalundo_button,
            CODE_BUTTON_BLUE_GOAL: core.easy_act_blue_goal_button,
            CODE_BUTTON_BLUE_UNDO: core.easy_act_blue_goalundo_button,
            }
        while running:
            code_str = self.request.recv(1)
            if not code_str:
                continue
            code = ord(code_str)
            if code != 0:
                print("Received code: %d" % (code), file=sys.stderr)
            # Probably there's a better way than reloading it from file constantly
            ignore_codes = reload_ignore_codes() or ignore_codes
            if code not in ignore_codes:
                with core_lock:
                    try:
                        if check_time(core.last_goal_time) > ANTISPAM_TIMER:
                            if dry_run:
                                print("fun: %s" % str(actions[code]), file=sys.stdout)
                            else:
                                actions[code]()
                    except KeyError:
                        print("Wrong code", file=sys.stderr)
            else:
                print("Ignore command because of configuration", file=sys.stderr)
            with core_lock:
                core.update()
            if check_time(core.last_goal_time) < SHOW_PARTIAL_TIME:
                red_score = core.easy_get_red_part()
                blue_score = core.easy_get_blue_part()
            else:
                red_score = core.easy_get_red_score()
                blue_score = core.easy_get_blue_score()
            self.request.send(struct.pack(">HH", red_score, blue_score))
        self.request.shutdown(socket.SHUT_RDWR)
        print("Connection closed", file=sys.stderr)


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


def main():
    global running, core, core_lock, allowed_IPs, dry_run

    parser = argparse.ArgumentParser(description='Arduino interface')
    parser.add_argument('match_id', type=int, help='The id of the match to connect to')
    parser.add_argument('--listen_addr', default='0.0.0.0', help='The address the listener server listens to')
    parser.add_argument('--listen_port', default=2204, type=int, help='The port the listener server listens to')
    parser.add_argument('arduino_ip', help='The ip of the allowed Arduino')
    parser.add_argument('--dry-run', action='store_true', help='Test run')

    args = parser.parse_args()

    allowed_IPs.append(args.arduino_ip)
    dry_run = args.dry_run

    core = SubottoCore(args.match_id)
    with core_lock:
        core.update()

    # Initialize ConnectionServer
    server = ThreadedTCPServer((args.listen_addr, args.listen_port), Connection)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    print("Started server on {}:{}".format(args.listen_addr, args.listen_port), file=sys.stdout)

    # Do things
    try:
        while True:
            with core_lock:
                core.update()
            time.sleep(1.0)
    except KeyboardInterrupt:
        running = False

    running = False
    server.shutdown()
    server.server_close()
    server_thread.join()


if __name__ == '__main__':
    main()
