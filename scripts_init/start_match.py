#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

from libsubotto.core import SubottoCore


def exit_usage():
    print("Usage: {} match_id [start|end]".format(sys.argv[0]))
    sys.exit(1)


def main():
    if len(sys.argv) != 3:
        exit_usage()

    match_id = int(sys.argv[1])
    action = sys.argv[2]

    core = SubottoCore(match_id)
    if action == "start":
        core.act_begin_match()
    elif action == "end":
        core.act_end_match()
    else:
        exit_usage()


if __name__ == '__main__':
    main()
