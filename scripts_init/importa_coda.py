import sys
import os
from fuzzywuzzy import fuzz

def usage():
    print("Usage:\npython3 importa_coda.py <match_id> <file.csv> <mode>")
    print("mode: 's' per la tabella del sito, 'q' per aggiungere in coda (non usare durante un cambio via GUI)")
    print("Ricorda di settare SUBOTTO_DB_URL")
    sys.exit(1)

if not os.environ["SUBOTTO_DB_URL"]:
    usage()

from libsubotto.data import Session, Player, Team
from libsubotto.core import SubottoCore

url = os.environ["SUBOTTO_DB_URL"]

# Controllo che i parametri siano sensati
if len(sys.argv) < 4:
    usage()

try:
    match_id = int(sys.argv[1])
    file = sys.argv[2]
    mode = sys.argv[3]
except (IndexError, ValueError):
    usage()

if not mode in ["q","s"]:
    usage()

# Inizializzo la classe che fa richieste al DB
core = SubottoCore(match_id)
core.update()
all_players = core.pull_all_players()

# Leggo il file
with open(file,"r") as f:
    lines = f.readlines()

print("Il formato atteso del file csv è:")
print("Matematico attacco, Matematico difesa, Fisico Attacco, Fisico Difesa, [orario 05:25]")
inp = input("Vuoi davvero procedere?(y/N)")
if inp not in ['y','Y','yes','YES','Yes']:
    sys.exit(1)


# Stringa da stampare per l'upload sul sito
site_string = []

# Stampa il nome bello, la funzione di libsubotto dà "cognome nome"
def cute_name(player):
    if player.comment:
        return f"{player.fname} {player.lname} {player.comment}"
    else:
        return f"{player.fname} {player.lname}"


# Definisco i teams
math = core.session.query(Team).filter(Team.id == 1).one()
phys = core.session.query(Team).filter(Team.id == 2).one()


# Eseguo lo script su ogni riga del file
for l in lines:
    l = l.replace('\n','')

    line = l.split(', ')
    if not len(line) in (4,5):
        print()
        print("Una riga non contiene il giusto numero di virgole, patchala", file=sys.stderr)
        print(l)
        sys.exit(1)

    time = "xx:xx"

    if len(line) == 5:
        time = line[4]
        line = line[:-1]

    # Trovo la squadra in base alla posizione del giocatore nel file
    iteration = 0
    teams = [math,math,phys,phys]
    players = []

    for person in line:
        team = teams[iteration]
        iteration += 1

        found = False
        name = person

        while not found:
            # Ordino i giocatori in base alla somiglianza con il nome cercato
            all_players.sort(reverse=True,key=(lambda q: fuzz.ratio(name, cute_name(q))))
            if cute_name(all_players[0]) == name:
                player = all_players[0]
                # Creo il player_match
                players.append(core.act_add_player_match_from_name(team, player.fname,
                            player.lname, player.comment))
                found = True
                continue

            print('---')
            print(f"{name}:")
            print()
            for i in range(10):
                print(f"{i}:  {cute_name(all_players[i])}")
            inp = input("Quale scegli? (numero per scegliere, 'n' se nuovo giocatore, 'm' per cercare con un altro nome):\n")

            if inp == 'n':
                splitname = name.split(' ')
                fname = ' '.join(splitname[:-1])
                lname = splitname[-1]
                comment = ''
                while True:
                    print("È corretta la seguente separazione?")
                    print(f"Nome: {fname}, Cognome: {lname}, Commento: {comment}")
                    inp = input("(y/n/exit)")
                    if inp in ['y','Y','yes','Yes','YES']:
                        # Creo il player e il player_match
                        if comment == '':
                            real_comment = None
                        else:
                            real_comment = comment
                        players.append(core.act_add_player_match_from_name(team, fname,
                                lname, real_comment))
                        found = True
                        break
                    elif inp in ['n','N','no','No','NO']:
                        print("Specifica i dati corretti da inserire")
                        fname = input("Nome: ")
                        lname = input("Cognome: ")
                        comment = input("Commento: ")
                    elif inp == 'exit':
                        break
                    else:
                        print("Input non valido, 'y' per sì, 'n' per no, 'exit' per tornare alla ricerca senza creare il player")

            elif inp == 'm':
                name = input("Inserisci a mano il nome completo vero da cercare:\n")
                continue

            else:
                try:
                    n = int(inp)
                except ValueError:
                    print("Il valore inserito non è valido, sono ammessi 'n', 'm' e i numeri da 0 a 9")
                    continue
                if n > 9 or n < 0:
                    print("Il valore inserito non è valido, sono ammessi 'n', 'm' e i numeri da 0 a 9")
                    continue

                player = all_players[n]
                # Creo il player_match e ritorno il player
                players.append(core.act_add_player_match_from_name(team, player.fname,
                            player.lname, player.comment))
                found = True
    if mode == 'q':
        core.act_add_to_queue(math, players[0], players[1])
        core.act_add_to_queue(phys, players[2], players[3])
        print("Aggiunti alla coda:")
        print(", ".join(cute_name(players[k]) for k in range(4)))

    elif mode == 's':
        site_string.append(f"<tr><td>{time}</td><td>{cute_name(players[0])}, {cute_name(players[1])}</td><td>{cute_name(players[2])}, {cute_name(players[3])}</td></tr>")

if mode == "s":
    print('\n'.join(site_string))

