#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import datetime

from libsubotto.core import act_init_match


def main():
    if len(sys.argv) < 4:
        print("Usage: {} match_name sched_begin sched_end [year]".format(sys.argv[0]))
        sys.exit(1)

    name = sys.argv[1]
    sched_begin = datetime.datetime.strptime(sys.argv[2], '%Y-%m-%d %H:%M:%S')
    sched_end = datetime.datetime.strptime(sys.argv[3], '%Y-%m-%d %H:%M:%S')
    year = None
    if len(sys.argv) == 5:
        year = int(sys.argv[4])
    match_id = act_init_match(name, "Matematici", "Fisici", sched_begin, sched_end, year=year)
    print("> Created match with ID {}".format(match_id))


if __name__ == '__main__':
    main()
