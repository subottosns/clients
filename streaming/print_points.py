#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""This script prints the current points to file"""

import sys
import time

from libsubotto.core import SubottoCore

def get_str_name(player):
    return "{} {}".format(player.fname, player.lname)

if __name__ == "__main__":
    match_id = int(sys.argv[1])
    core = SubottoCore(match_id)
    while True:
        core.update()

        with open("red_points.txt", "w") as outfile:
            outfile.write(str(core.easy_get_red_score()))

        with open("blue_points.txt", "w") as outfile:
            outfile.write(str(core.easy_get_blue_score()))

        with open("red_points_part.txt", "w") as outfile:
            outfile.write(str(core.easy_get_red_part()))

        with open("blue_points_part.txt", "w") as outfile:
            outfile.write(str(core.easy_get_blue_part()))

        with open("red_players.txt", "w") as outfile:
            outfile.write(get_str_name(core.players[core.detect_team(core.easy_get_red_team())][1]) + ', ' + get_str_name(core.players[core.detect_team(core.easy_get_red_team())][0]))

        with open("blue_players.txt", "w") as outfile:
            outfile.write(get_str_name(core.players[core.detect_team(core.easy_get_blue_team())][0]) + ', ' + get_str_name(core.players[core.detect_team(core.easy_get_blue_team())][1]))

        time.sleep(1)
