# Scripts per le statistiche
Dopo la 24h ci sono da fare le statistiche. Nessuno ha mai voglia, ma vanno
fatte comunque. Gli script in questa cartella servono a quello.

# Workfolw generale
Nel db ci sono tutti gli eventi della 24 ore, e a partire da questi vengono
calcolate le statistiche. Ovviamente questi eventi contengono un sacco di
errori (eg. le fotocellule impazziscono e segnano gol a caso, o i mitici cambi
in ritardo), ma la filosofia è sempre stata quella di _non_ correggerli. Quello
che si è sempre fatto invece è di correggere solo le statistiche, che poi
vengono caricate nel db giuste, anche se discordanti con gli eventi.

Questo viene fatto prendendo i dati dalla tabella eventi e creando il
cosiddetto "file dei turni", che viene poi corretto a mano. Dopo la procedura,
questo file contiene tutti i turni effettivamente giocati, con tempo di inizio
e di fine, punteggio, etc. ed è da considerare la fonte della verità assoluta
per quanto riguarda le statistiche di una 24 ore. Alla fine, il file dei turni
viene caricato sul db per generare le tabelle delle statistiche e archiviato
per referenza futura.

NOTA: da qua in poi è scritto a memoria dopo un anno e mezzo, è probabile che
manchi della roba e altro sia sbagliato. Da sistemare.

I passi che si seguono durante le statistiche di solito sono questi
(descrizioni più accurate dopo):
- unire i giocatori
- creare il file dei turni
- correggere il file dei turni a mano
- fare un backup del db
- caricare i turni nel db
- archiviare il file dei turni

## Unire i giocatori
Succede sempre che ci siano persone già nel db ma che vengono aggiunge di nuovo
(es. Edoardo Maria Centamori, che credo sia stato riaggiungo come Edoardo
Centamori in ogni singola edizione a cui ha partecipato). Questo problema
dovrebbe esserci meno con la nuova GUI, but still due righe su come sistemare
non fanno male.

Al momento la cosa è molto manuale e poco automatica: si accede al db nella
tabella dei giocatori (`player`?), si identificano il giocatore "vero" e quello
da mergeare, si annotano i loro id, si invoca lo script `convert_player.py
<id_to_merge> <id_merge_into>` che stampa a schermo delle query. Le prime due
dovrebbero sistemare gli eventi, le altre due stampano qualcosa, tipo per
controllare che i due giocatori non siano distinti perché quella persona ha
giocato con entrambe le squadre in quell'anno.

Boh, da capire meglio

## Creare il file dei turni
Per creare il file dei turni basta usare lo script `find_turns.py <id_24h>
[filename]`. Il nome del file è opzionale (default `turns<anno>.csv`) ed è il
file che viene creato con i turni. Se il file esiste già probabilmente viene
troncato, ma forse lo script appende. Questo script è in sola lettura sul db.

## Correggere il file dei turni a mano
Il file dei turni generato dallo script è solitamente pieno di errori. Per
correggerli viene revisionato a mano, usando la memoria dei presenti, i video e
un po' di buon senso. Dovrebbe anche esserci un regolamento preciso su cosa si
può e non si può fare durante questa fase. La cosa dei turni multipli di 10
palline aiuta molto ad indentificare i turni problematici, di solito i problemi
sono in quasi tutte e quasi sole le righe in cui la somma non fa 0 mod 10.

## Fare un backup del db
Comando `pg_dump`, guardati la documentazione.

## Caricare i turni nel db
Per questa fase si usa lo script `produce_statistics.py <id_24h> <filename>`.
L'id è quello della 24h, filename è quello del file dei turni. Di base lo
script dovrebbe stampare le informazioni dei giocatori, mentre le righe che
operano davvero sul db sono commentate. Per fare davvero le cose, decommentarle
e poi eseguire lo script.

Sono quasi sicuro che questo script __NON__ sia idempotente. È probabile che
aggiunga solo elementi con il match_id della 24h considerata in tabelle che non
vengono toccate da nient'altro, quindi eliminare il suo effetto non _dovrebbe_
essere troppo difficile, ma è qualcosa da guardare meglio.

## Archiviare il file dei turni
Anche se in teoria è possibile ricostruirlo dalle statistiche nel db, è buona
norma archiviare quello corretto durante la (lunga e faticosa) sessione di
statistiche. Al momento sono sulla common in `misc/24Ore/file_dei_turni`.

# TODO
- invece di commentare e scommentare le righe in `produce_statistics.py`,
  chiedere all'utente (magari con doppia conferma default su no)
- rendere idempotente lo script `produce_statistics.py`
- capire come funziona unire i giocatori
- tra il 2021 e il 2022 i file dei turni sono cambiati di formato per usare gli
  id dei player_match invece dei player (70b79a19c540f2604c11d804b1199d88863024bb).
  Questo vuol dire che i file più vecchi non sono più compatibili. Bisognerebe
  fixarli.