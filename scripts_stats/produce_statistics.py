# -*- coding: utf-8 -*-
"""Script to update statistics of a 24h."""

from libsubotto.data import Session, PlayerMatch, StatsTurn, StatsPlayerMatch

import sys
import logging
import csv
import datetime

logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s - %(message)s"
)
logger = logging.getLogger()


class Turn:
    """A single turn, ie. the time between two consecutive swaps."""

    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

    def __init__(self, line, all_player_matches):
        """Create a new Turn from a CSV line."""

        def get_player_match(player_match_id):
            player_match = next(k for k in all_player_matches if k.id == int(player_match_id))
            return player_match

        self.player_matches = (
            None,
            (get_player_match(line[2]), get_player_match(line[3])),
            (get_player_match(line[4]), get_player_match(line[5]))
        )
        self.score = (None, int(line[6]), int(line[7]))
        self.cumulative_score = (None, int(line[8]), int(line[9]))
        self.begin = datetime.datetime.strptime(line[0], Turn.DATETIME_FORMAT)
        self.end = datetime.datetime.strptime(line[1], Turn.DATETIME_FORMAT)

    def __str__(self):
        return "Turn: {} - {}".format(self.begin, self.end)

    def __repr__(self):
        return "\n(" + ",".join(map(str, (
            self.player_matches[1][0].player.format_name(),
            self.player_matches[1][1].player.format_name(),
            self.player_matches[2][0].player.format_name(),
            self.player_matches[2][1].player.format_name(),
            self.score[1],
            self.score[2],
            self.begin,
            self.end
        ))) + ")"

    def duration(self):
        """Compute duration of this turn in seconds."""
        return int((self.end - self.begin).total_seconds())

    def get_stats_turn(self, match_id):
        """Get a DB StatsTurn object for this instance."""
        return StatsTurn(match_id=match_id,
                         p00_id=self.player_matches[1][0].player_id,
                         p01_id=self.player_matches[1][1].player_id,
                         p10_id=self.player_matches[2][0].player_id,
                         p11_id=self.player_matches[2][1].player_id,
                         score_a=self.score[1],
                         score_b=self.score[2],
                         begin=self.begin,
                         end=self.end)


class PlayerMatchInfo():
    """Player's informations for this partecipation."""

    def __init__(self, player_match, team):
        # First get the player from player_match_id and team
        self.id = player_match.id
        assert player_match.team.id == team
        self.player = player_match.player
        self.team = team
        self.pos_goals = 0
        self.neg_goals = 0
        self.seconds = 0
        self.turns = 0

    def __repr__(self):
        return ("("
                + ",".join(map(str, (
                    self.id,
                    self.player.format_name(),
                    self.team,
                    self.pos_goals,
                    self.neg_goals,
                    self.seconds,
                    self.turns
                )))
                + ")")

    def get_stats_player_match(self, match_id):
        """Get a DB StatsPlayerMatch object for this instance."""
        return StatsPlayerMatch(player_id=self.player.id,
                                match_id=match_id,
                                team_id=self.team,
                                pos_goals=self.pos_goals,
                                neg_goals=self.neg_goals,
                                seconds=self.seconds,
                                turns=self.turns)


def usage():
    """Print usage and exit."""
    print("Usage: {} match_id filename".format(sys.argv[0]))
    sys.exit(1)

def ask_confirm(prompt):
    answer = input(prompt + " [y/N] ")
    if answer.lower() in ["y","yes"]:
        return True
    else:
        return False

if __name__ == "__main__":
    try:
        match_id = int(sys.argv[1])
        filename = sys.argv[2]
    except (IndexError, ValueError):
        usage()

    session = Session()

    try:
        # Check if there are already statistics for this match
        stats_turns = session.query(StatsTurn).filter_by(match_id=match_id)
        stats_player_matches = session.query(StatsPlayerMatch).filter_by(match_id=match_id)
        if stats_turns.first() is not None or stats_player_matches.first() is not None:
            # If there are, prompt the user to delete them or to abort
            print(f"WARNING: there are already statistics for match {match_id} in the database")
            cont = False
            if ask_confirm("Do you want to delete them?"):
                # Actually delete things
                stats_turns.delete()
                stats_player_matches.delete()
                session.commit()
                print("Previous statistics deleted")
                if not ask_confirm("Continue uploading the new statistics?"):
                    exit(0)
            else:
                print("Aborting")
                session.rollback()
                exit(-1)
    except Exception as e:
        # If anything goes wrong abort operations on the db and propagate the
        # exception
        logger.error("Errore nell'elaborazione: annullo ogni operazione sul DB")
        session.rollback()
        raise e

    # Actually compute and upload statistics
    players = {}

    # Get all the relevant player matches. Used later to get the player_id, but
    # also for pretty printing
    all_player_matches = session.query(PlayerMatch).filter_by(match_id=match_id).all()

    try:
        # Load turns from csv file
        turns = []
        with open(filename, "r") as csvfile:
            csvreader = csv.reader(csvfile, delimiter=",", quotechar="'")
            for row in csvreader:
                turn = Turn(row, all_player_matches)
                turns.append(turn)
                for team in (1, 2):
                    for player_match in turn.player_matches[team]:
                        if player_match.id not in players:
                            players[player_match.id] = PlayerMatchInfo(player_match, team)
        print(str(turns))

        # Process turns
        final_score = [None, 0, 0]
        for turn in turns:
            for team in (1, 2):
                other_team = 1 if team == 2 else 2
                final_score[team] += turn.score[team]
                for player_match in turn.player_matches[team]:
                    players[player_match.id].pos_goals += turn.score[team]
                    players[player_match.id].neg_goals += turn.score[other_team]
                    players[player_match.id].turns += 1
                    players[player_match.id].seconds += turn.duration()
        print(players.values())
        print("Punteggio finale: ", final_score[1], final_score[2])

        # Actual writing on the db
        if ask_confirm("Apply changes to the database?"):
            if ask_confirm("Are you really sure?"):
                session.add_all(map(lambda turn: turn.get_stats_turn(match_id), turns))
                session.add_all(map(lambda pl: pl.get_stats_player_match(match_id), players.values()))
                session.commit()
                print("Statistics saved in the database")
    except Exception as e:
        # If anything goes wrong abort operations on the db and propagate the
        # exception
        logger.error("Errore nell'elaborazione: annullo ogni operazione sul DB")
        session.rollback()
        raise e

    session.close()
