#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""This module builds and runs the GUI, a Gtk3 desktop application"""

import sys
import os

from libsubotto.core import SubottoCore
from libsubotto.data import Event

try:
    import gi
    # TODO: really port the GUI to Gtk 3.0
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
except ImportError:
    print("Could not load GTK3 packages")
    sys.exit()

debuglevel = 0


def debug(string, level):
    if debuglevel >= level:
        print(string)


class SquadraSubotto(object):

    def __init__(self, team, core, glade_file="subotto24.glade"):
        self.glade_file=glade_file
        self.team = team
        self.core = core
        self.builder = Gtk.Builder()
        self.builder.add_objects_from_file(glade_file, ["box_team", "im_queue_promote", "im_gol_plus", "im_gol_minus",
                                                        "im_to_queue", "im_swap_up", "im_swap_down", "im_add_player",
                                                        "im_delete_queue"])
        self.core.listeners.append(self)

        self.box = self.builder.get_object("box_team")
        self.name_gtk = self.builder.get_object("name_team")
        self.name_gtk.set_text(self.team.name)
        self.builder.connect_signals(self)

        #This was needed in old GUI
        #self.player_list = []
        #self.player_map = {}
        #self.combo_players = Gtk.ListStore(int, str)
        #combo_renderer = Gtk.CellRendererText()
        #self.builder.get_object("combo_queue_att").set_model(self.combo_players)
        #self.builder.get_object("combo_queue_att").pack_start(combo_renderer, True)
        #self.builder.get_object("combo_queue_att").add_attribute(combo_renderer, 'text', 1)
        #self.builder.get_object("combo_queue_dif").set_model(self.combo_players)
        #self.builder.get_object("combo_queue_dif").pack_start(combo_renderer, True)
        #self.builder.get_object("combo_queue_dif").add_attribute(combo_renderer, 'text', 1)
        #self.combos = [self.builder.get_object("combo_queue_att"),
                       #self.builder.get_object("combo_queue_dif")]

        self.treeview_queue = self.builder.get_object("treeview_queue")
        print(self.treeview_queue)
        self.treeview_queue.append_column(Gtk.TreeViewColumn("Attaccante", Gtk.CellRendererText(), text=0))
        self.treeview_queue.append_column(Gtk.TreeViewColumn("Difensore", Gtk.CellRendererText(), text=1))
        self.queue_model = Gtk.ListStore(str, str)
        self.treeview_queue.set_model(self.queue_model)
        self.queue_cache = None
        self.list_of_added = []
        #At the beginning, we need to pull all existing players from DB
        self.list_from_db = self.core.pull_all_players()
        for player in self.list_from_db:
            self.list_of_added.append(tuple([player.lname, player.fname, player.comment]))
        self.list_of_added = sorted(self.list_of_added, key=lambda k: (k[0] is not None, k[0] != '', k[0], k[1] is not None, k[1] != '', k[1], k[2] is not None, k[2] != '', k[2]))

        self.window_new_player = self.builder.get_object("window_new_player")
        print(self.window_new_player)
        self.entry_fname = self.builder.get_object("entry_fname")
        self.entry_lname = self.builder.get_object("entry_lname")
        self.entry_comment = self.builder.get_object("entry_comment")
        #gets the selected player
        #self.new_selection = self.window_new_player.get_selection()
        
        #Treeview to display players matching input
        #I think the text= tells the column number. Useful later to retrieve the data when row is clicked.
        self.treeview_new = self.builder.get_object("treeview_new")
        self.treeview_new.append_column(Gtk.TreeViewColumn("Cognome", Gtk.CellRendererText(), text=0))
        self.treeview_new.append_column(Gtk.TreeViewColumn("Nome", Gtk.CellRendererText(), text=1))
        self.treeview_new.append_column(Gtk.TreeViewColumn("Commento", Gtk.CellRendererText(), text=2))
        self.new_model = Gtk.ListStore(str, str, str)
        self.treeview_new.set_model(self.new_model)
        
        #treeview to display candidate attacker
        self.candidate_atk = self.builder.get_object("candidate_atk")
        self.candidate_atk.append_column(Gtk.TreeViewColumn("Attaccante", Gtk.CellRendererText(), text=0))
        self.atk_model = Gtk.ListStore(str)
        self.candidate_atk.set_model(self.atk_model)
        
        #treeview to display candidate defender
        self.candidate_def = self.builder.get_object("candidate_def")
        self.candidate_def.append_column(Gtk.TreeViewColumn("Difensore", Gtk.CellRendererText(), text=0))
        self.def_model = Gtk.ListStore(str)
        self.candidate_def.set_model(self.def_model)

    def goal_incr(self):
        self.core.act_goal(self.team)

    def goal_decr(self):
        self.core.act_goal_undo(self.team)

    #This worked with old-styled GUI
    #def to_queue(self):
        #player_a, player_b = [self.player_map[x.get_model()[x.get_active()][0]] if x.get_active() >= 0 else None for x in self.combos]
        #if player_a is None or player_b is None:
            #print("> Cannot move to queue when one of the players is not chosen...", file=sys.stderr)
        #else:
            #self.core.act_add_to_queue(self.team, player_a, player_b)

    def to_queue(self):
        try:
            if self.atk_to_queue is None or self.def_to_queue is None:
                print("> Cannot move to queue when one of the players is not chosen...", file=sys.stderr)
            else:
                self.core.act_add_to_queue(self.team, self.atk_to_queue, self.def_to_queue)
                self.atk_model.clear()
                self.atk_to_quque = None
                self.def_model.clear()
                self.def_to_queue = None
        except:
            print("> Cannot move to queue when one of the players is not chosen...")
            
    def promote(self):
        if len(self.core.queues[self.core.detect_team(self.team)]) == 0:
            print("> Cannot promote when queue is empty", file=sys.stderr)
        else:
            player_a, player_b = self.core.queues[self.core.detect_team(self.team)][0]
            self.core.act_team_change(self.team, player_a, player_b)
            self.core.act_remove_from_queue(self.team, 0)

    def swap(self, first, second):
        length = len(self.queue_model)
        if first >= 0 and second >= 0 and first < length and second < length:
            self.core.act_swap_queue(self.team, first, second)
            self.set_selection_index(second)

    def swap_up(self):
        sel = self.get_selection_index()
        if sel is None:
            print("> Error: no selection when swapping queue", file=sys.stderr)
        else:
            self.swap(sel, sel-1)

    def swap_down(self):
        sel = self.get_selection_index()
        if sel is None:
            print("> Error: no selection when swapping queue", file=sys.stderr)
        else:
            self.swap(sel, sel+1)

    def delete_queue(self):
        sel = self.get_selection_index()
        if sel is None:
            print("> Error: no selection when deleting queue", file=sys.stderr)
        else:
            self.core.act_remove_from_queue(self.team, sel)

    #That window doesn't exist any longer
    #def add_player(self):
        #self.entry_fname.set_text('')
        #self.entry_lname.set_text('')
        #self.entry_comment.set_text('')
        #self.window_new_player.show()
        #self.entry_fname.grab_focus()
        
    def none_to_end_key(item):
        value = item.data.value if item.data else None
        return (value is None, value)
        
    #finds players matching inserted data when name is changed and prints list. Experimental.
    def name_changed_find_matching(self, widget):
        #First, let's get stuff
        fname = self.entry_fname.get_text()
        lname = self.entry_lname.get_text()
        comment = self.entry_comment.get_text()
        if comment == '':
            comment = None
        #Then, let's print the list
        if comment == None:
            matching = [x for x in self.list_of_added if x[0].lower().startswith(lname.lower()) and x[1].lower().startswith(fname.lower())]
        else:
            commented = [x for x in self.list_of_added if x[0].lower().startswith(lname.lower()) and x[1].lower().startswith(fname.lower()) and x[2] != None]
            matching = [x for x in commented if x[2].lower().startswith(comment.lower())]
        self.new_model.clear()
        for candidate in matching:
            self.new_model.append(tuple([x for x in candidate]))
    
    #triggered when player is clicked on TreeView
    def on_selected_player(self, widget, row, column):
        self.x0=self.new_model[row][0]
        self.x1=self.new_model[row][1]
        self.x2=self.new_model[row][2]
        #If we try something like self.entry_fname.set_text(self.new_model[row][0])
        #and company, sometimes it gets stuck on row indexes >=3,
        #even though, if we ask to print the data of all the columns before reading
        #before reading from treeview and writing to another treeview, it correctly
        #prints to terminal. This is weird.
        #Assigning x0, x1, x2 avoids it. I think it is a bug in the library 
        #that handles the widgets. This workaround semms to work.
        self.entry_lname.set_text(self.x0)
        self.entry_fname.set_text(self.x1)
        if self.x2 == None:
            self.entry_comment.set_text('')
        else:
            self.entry_comment.set_text(self.x2)
        
    def on_btn_new_atk_ok_clicked(self, widget):
        self.candidate_atk_fname = self.entry_fname.get_text()
        self.candidate_atk_lname = self.entry_lname.get_text()
        self.candidate_atk_comment = self.entry_comment.get_text()
        if self.candidate_atk_comment == '':
            self.candidate_atk_comment = None
        self.candidate_role=0
        self.typo_warning=1
        self.typo_check()
        if self.typo_warning==0:
            self.atk_ok_enact()

    def atk_ok_enact(self):
        #print("Chiamato?")
        player = self.core.act_add_player_match_from_name(self.team, self.candidate_atk_fname, self.candidate_atk_lname, self.candidate_atk_comment)
        #print("Trovato!")
        self.atk_model.clear()
        self.atk_model.append(tuple([player.format_name()]))
        self.entry_fname.set_text('')
        self.entry_lname.set_text('')
        self.entry_comment.set_text('')
        self.atk_to_queue = player
        
    def on_btn_new_def_ok_clicked(self, widget):
        self.candidate_def_fname = self.entry_fname.get_text()
        self.candidate_def_lname = self.entry_lname.get_text()
        self.candidate_def_comment = self.entry_comment.get_text()
        if self.candidate_def_comment == '':
            self.candidate_def_comment = None
        self.candidate_role=1
        self.typo_warning=1
        self.typo_check()
        if self.typo_warning==0:
            self.def_ok_enact()

    def def_ok_enact(self):
        player = self.core.act_add_player_match_from_name(self.team, self.candidate_def_fname, self.candidate_def_lname, self.candidate_def_comment)
        self.def_model.clear()
        self.def_model.append(tuple([player.format_name()]))
        self.entry_fname.set_text('')
        self.entry_lname.set_text('')
        self.entry_comment.set_text('')
        self.def_to_queue = player
        
    def on_btn_new_player_cancel_clicked(self, widget):
        #self.window_new_player.hide()
        self.entry_fname.set_text('')
        self.entry_lname.set_text('')
        self.entry_comment.set_text('')

    def typo_check(self):
            if self.candidate_role==0:
                self.list_of_existing_and_completely_matching = []
                self.list_of_existing_and_completely_matching = [x for x in self.list_of_added if x[0] == self.candidate_atk_lname and x[1] == self.candidate_atk_fname and x[2] == self.candidate_atk_comment]
            elif self.candidate_role==1:
                self.list_of_existing_and_completely_matching = []
                self.list_of_existing_and_completely_matching = [x for x in self.list_of_added if x[0] == self.candidate_def_lname and x[1] == self.candidate_def_fname and x[2] == self.candidate_def_comment]
            else:
                print("Bad role. Internal code error.")
            
            if self.list_of_existing_and_completely_matching == []:
                
                #Per rendere il codice più pulito, questa finestra va resa una classe a sé.
                #Create pop-up window
                self.popup_window = Gtk.Window(type=Gtk.WindowType.TOPLEVEL)
                self.popup_window.set_title("Conferma nuovo giocatore")
                self.popup_window.set_modal(True) #Questo non so davvero a cos serva, ma non sembra far danni
                self.popup_window.set_position(Gtk.WindowPosition.CENTER_ON_PARENT)
                self.popup_grid=Gtk.Grid()
                self.popup_window.add(self.popup_grid)
                self.popup_button1 = Gtk.Button(label="Annulla")
                self.popup_button1.connect("clicked", self.on_btn_new_player_abort)
                self.popup_grid.attach(self.popup_button1, 0, 2, 1, 1)
                self.popup_button2 = Gtk.Button(label="Conferma")
                self.popup_button2.connect("clicked", self.on_btn_new_palyer_confirm)
                self.popup_grid.attach(self.popup_button2, 1, 2, 1, 1)
                self.popup_label_warning=Gtk.Label(label="Stai per creare il giocatore")
                self.popup_label_warning.set_justify(Gtk.Justification.CENTER)
                self.popup_grid.attach(self.popup_label_warning, 0,0,2,1)
                self.popup_label_descriptors=Gtk.Label(label="Cognome: \nNome: \nCommento: ")
                self.popup_label_descriptors.set_justify(Gtk.Justification.RIGHT) 
                self.popup_grid.attach(self.popup_label_descriptors, 0,1,1,1)
                self.popup_label_info_raw="{}\n{}\n{}"
                print("Cusu")
                if self.candidate_role==0:
                    self.popup_label_info_formatted=self.popup_label_info_raw.format(self.candidate_atk_lname, self.candidate_atk_fname, self.candidate_atk_comment)
                elif self.candidate_role==1:
                    self.popup_label_info_formatted=self.popup_label_info_raw.format(self.candidate_def_lname, self.candidate_def_fname, self.candidate_def_comment)
                else:
                    print("Bad role. Internal code error.")
                print("mano")
                self.popup_label_info=Gtk.Label(label=self.popup_label_info_formatted)
                self.popup_label_info.set_justify(Gtk.Justification.LEFT)
                self.popup_grid.attach(self.popup_label_info, 1,1,1,1)
                self.popup_window.show_all()
            else:
                self.typo_warning=0
    
    def on_btn_new_palyer_confirm(self, widget):
        if self.candidate_role == 0:
            self.atk_ok_enact()
            self.popup_window.destroy()
        elif self.candidate_role == 1:
            self.def_ok_enact()
            self.popup_window.destroy()
        else:
            print("Il ruolo del nuovo giocatore non è in {0,1}. Non dovrebbe capitare.")

    def on_btn_new_player_abort(self, widget):
        #print("Cusu")
        self.popup_window.destroy()

    #def on_window_new_player_delete_event(self, widget, event):
        #self.window_new_player.hide()
        #return True

    def on_btn_gol_plus_clicked(self, widget):
        self.goal_incr()

    def on_btn_gol_minus_clicked(self, widget):
        self.goal_decr()

    def on_btn_to_queue_clicked(self, widget):
        self.to_queue()

    def on_btn_queue_promote_clicked(self, widget):
        self.promote()

    def on_btn_swap_up_clicked(self, widget):
        self.swap_up()

    def on_btn_swap_down_clicked(self, widget):
        self.swap_down()

    def on_btn_add_player_clicked(self, widget):
        self.add_player()

    def on_btn_delete_queue_clicked(self, widget):
        self.delete_queue()

    def get_selection_index(self):
        selection = self.treeview_queue.get_selection()
        for i in range(len(self.queue_model)):
            if selection.iter_is_selected(self.treeview_queue.get_model().get_iter(Gtk.TreePath(i))):
                return i

    def set_selection_index(self, idx):
        selection = self.treeview_queue.get_selection()
        selection.unselect_all()
        selection.select_iter(self.treeview_queue.get_model().get_iter(Gtk.TreePath(idx)))

    def regenerate(self):
        # Write score
        self.builder.get_object("points").set_text("%d" % (self.core.score[self.core.detect_team(self.team)]))

        # Write active players
        players = self.core.players[self.core.detect_team(self.team)]
        if players != [None, None]:
            for i in (("name_current_att", 0), ("name_current_dif", 1)):
                self.builder.get_object(i[0]).set_text(players[i[1]].format_name())

        # Write queue
        if self.queue_cache != self.core.queues[self.core.detect_team(self.team)]:
            sel_idx = self.get_selection_index()
            self.queue_cache = self.core.queues[self.core.detect_team(self.team)]
            self.queue_model.clear()
            for queue_element in self.core.queues[self.core.detect_team(self.team)]:
                self.queue_model.append(tuple([x.format_name() for x in queue_element]))
            if sel_idx is not None and sel_idx < len(self.queue_model):
                self.set_selection_index(sel_idx)

    def new_player_match(self, player_match):
        # Update player combo boxes
        #if player_match.team == self.team:
            #player = player_match.player
            # TODO - Bad hack, because I can't work out how to keep
            # the list sorted in a nicer way
            #self.player_list.append((player.format_name(), player.id))
            #self.player_list.sort()
            #self.player_map[player.id] = player
            #self.combo_players.clear()
            #for i, j in self.player_list:
                #self.combo_players.append((j, i))
        pass

    def new_event(self, event):
        pass


class Subotto24GTK(object):

    team = dict()
    team_slot = dict()

    def __init__(self, core, glade_file="subotto24.glade"):
        self.builder = Gtk.Builder()
        self.builder.add_objects_from_file(glade_file, ["window_subotto_main", "im_team_switch"])
        self.core = core
        self.core.listeners.append(self)

        self.window = self.builder.get_object("window_subotto_main")

        self.team_slot = [self.builder.get_object("box_red_slot"),
                          self.builder.get_object("box_blue_slot")]
        self.teams = [SquadraSubotto(self.core.match.team_a, core, glade_file),
                      SquadraSubotto(self.core.match.team_b, core, glade_file)]
        self.order = [None, None]
        
        for team in self.teams:
            #this is to have the list displayed when GUI is opened, otherwise it would appear when something is typed
            #the line below forces a refresh
            team.name_changed_find_matching(team.treeview_queue)

        self.builder.connect_signals(self)

    def switch_teams(self):
        self.core.act_switch_teams()
        self.update_teams()

    def update_teams(self):
        # Update our internal copy of order
        if self.core.order == [None, None]:
            self.order = [None, None]
        else:
            if self.core.order[0] == self.core.teams[0]:
                self.order = [self.teams[0], self.teams[1]]
            else:
                self.order = [self.teams[1], self.teams[0]]

            # Update GUI and teams
            for i in [0, 1]:
                parent = self.order[i].box.get_parent()
                if parent is not None:
                    parent.remove(self.order[i].box)
                self.team_slot[i].add(self.order[i].box)

    def on_window_destroy(self, widget):
        self.core.close()
        Gtk.main_quit()

    def on_btn_switch_clicked(self, widget):
        debug("Cambio!", 2)
        self.switch_teams()

    def regenerate(self):
        debug("Updating!", 20)
        #self.update_teams()

    def new_player_match(self, player_match):
        player = player_match.player
        #adds player to the list of available players
        for team in self.teams:
            team.check_added = [x for x in team.list_of_added if x[0] == player.lname and x[1] == player.fname and x[2] == player.comment]
            #the condition below is to avoid that a player is added twice, as it would happen if the GUI instance was started after a new player has been added:
            #they would be added twice, once when pullng the DB and once when dealing with the "new player_match" event,
            #which is issued also for things happend before the GUI instance was started
            if team.check_added == []:
                team.list_of_added.append(tuple([player.lname, player.fname, player.comment]))
                #sorts the list
                team.list_of_added = sorted(team.list_of_added, key=lambda k: (k[0] is not None, k[0] != '', k[0], k[1] is not None, k[1] != '', k[1], k[2] is not None, k[2] != '', k[2]))
                #forces a refresh of displayed players. If this doesn't happen, new players aren't displaued until input changes
                team.name_changed_find_matching(team.treeview_queue)
        #pass

    def new_event(self, event):
        if event.type == Event.EV_TYPE_SWAP:
            self.update_teams()


if __name__ == "__main__":

    match_id = int(sys.argv[1])
    core = SubottoCore(match_id)

    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'subotto24.glade')
    main_window = Subotto24GTK(core, glade_file=filename)

    core.update()
    gi.repository.GLib.timeout_add(1000, core.update)

    Gtk.main()
