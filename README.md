# Subotto clients & scripts

Installare i requirements; inoltre è necessario poter importare libsubotto, vedere il readme relativo per l'installazione.

## GUI
Applicazione desktop per gestire la coda ed eventualmente inserire gol a mano

### Dipendenze

 * Installare con apt i pacchetti `libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0`
 * Installare con pip i pacchetti `pycairo PyGObject`

## Arduino
Programma .ino dell'Arduino e relativa interfaccia con python e il DB

## Scripts per la gestione
Utili scripts per iniziare/finire un match, inserire i giocatori e altro

## Scripts per le statistiche
Scripts per la creazione delle statistiche a 24ore finita

# Preparare una 24h
* Su tutti i pc che vogliono parlare con il db devi esportare `SUBOTTO_DB_URL` con la stringa di connessione al db (per esempio mettendolo nel `.bashrc`)
* Su tutti i pc creare un virtualenv e installarci `libsubotto`
* In generale vuoi scaricare anche `clients` (e installare i suoi requirements)
* Vai su `scripts_init` e lancia `python init_match.py` (argomenti etc. nella sua documentazione), che in risposta ti spara l'id della 24h appena creata
* Dati i file con i nomi dei partecipanti (un file per squadra) nel formato `nome,cognome[,commento]`, uno per riga, bisogna caricarli con `python load_team_names.py` (vedi la sua doc)
* Vai su soyuz e sistema il sito web (vedi il repo `website`)
  * I servizi rilevanti sono `24_{web,feeder}{,_test}`
  * In particolare dovrebbe bastare cambiare l'id della 24h di test su `24_feeder_test.service` oltre a quello che dice il README della repo, per il resto dovrebbe funzionare già tutto
* Apri la pagina web sul pc collegato al proiettore perché la gente vuole le GUI
* Apri la GUI dove va aperta (di solito almeno il pc collegato al proiettore, e forse anche su un altro). Aperta la GUI, lancia l'evento di swap (che inizia ogni 24h)
* Aggiungi le prime due coppie in coda, e mettile in campo prima che inizi la 24h
* Configurare l'arduino e l'interfaccia (vedi `arduino/README.md` per più dettagli):
  * Flashare l'arduino con l'ip della macchina su cui si avvierà `arduino_interface.py`
  * Eseguire `python arduino_interface.py <id 24h> <ip arduino>`
* Fai le cose con OBS nel modo giusto (gnegne)
* Configura YouTube per lo streaming, avvialo da OBS e poi Vai LIVE!
* Quando vuoi far partire la 24h lancia `python start_match <id 24h> start`
* Quando poi vuoi finire la 24h lancia `python start_match <id 24h> end`

## Avere 1 webcam per streaming e subtracker
* Guardare le videocamere attive con `ls /dev/video*`
* Capire in qualche modo quale si vuole usare (di solito sono 2 devices per webcam)
* Trovare il modulo del kernel v4l2loopback (su debian `v4l2loopback-dkms`)
* Caricarlo con `sudo modprobe v4l2loopback devices=2` (possono essere n)
* Serve ora streamare sulle webcam virtuali:
 ```ffmpeg -f v4l2 -i /dev/video0 -f v4l2 /dev/video3 -f v4l2 /dev/video4```
  In questo caso la 0 è quella vera (dei due devices provare il numero più basso, di solito funziona), mentre la 3 e la 4 sono quelle virtuali
* OBS dovrebbe vedere le due con nome `Dummy video` e cusumani vari, sceglierne una
* Per tracker lanciare il seguente comando dalla cartella "video" nel repo del vecchio subtracker
 ```ffmpeg -f v4l2 -i /dev/video3 -vcodec mjpeg -q:v 3 -f image2pipe - | ./mjpeg_source.py | ./monitor_tee.py 640 480```
 dove la 3 è la camera virtuale libera. I numeri alla fine sono le dimensioni dell'immagine da sputare, se sono sbagliati si rompe.
 I flag a caso sono copiati da subtracker vecchio e potrebbero non servire.
